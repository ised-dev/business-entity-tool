const webpack = require("webpack");
if (process.env.VUE_APP_MODE == "prod") {
  require("./env.prod.js");
} else {
  require("./env.js");
}
module.exports = {
  configureWebpack: {
    plugins: [
      new webpack.optimize.LimitChunkCountPlugin({
        maxChunks: 1
      })
    ]
  },
  chainWebpack: config => {
    config.optimization.delete("splitChunks");
  },
  filenameHashing: false,
  css: { extract: false } // include if you do not want a separate css file
};