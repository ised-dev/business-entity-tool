import Vue from "vue";
import App from "./App.vue";
import Buefy from "buefy";
import VueGtag from "vue-gtag";
import { library } from "@fortawesome/fontawesome-svg-core";
import {
  faTimes,
  faAngleDown,
  faAngleUp,
  faUndo,
  faExclamationCircle
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";

library.add(faTimes, faAngleDown, faAngleUp, faUndo, faExclamationCircle);

Vue.component("font-awesome-icon", FontAwesomeIcon);

Vue.use(Buefy, {
  defaultIconComponent: "font-awesome-icon"
});

Vue.use(VueGtag, {
  config: {
    id: process.env.VUE_APP_GA4TAG_ID || "G-9XEJCX5YB5", // default to LIVE GA4 ID
    params: {
      send_page_view: false
    }
  },
  includes: [
    {
      id: process.env.VUE_APP_GTAG_ID || "UA-21143245-13", // default to LIVE GA ID    ***old***
      params: {
        send_page_view: false
      }
    }
  ]
});

// Import customElement for Web component option with build-lib
import vueCustomElement from "vue-custom-element";
Vue.use(vueCustomElement);
Vue.customElement("business-entity-tool", App);
Vue.config.productionTip = false;

new Vue({
  render: h => h(App)
}).$mount("#app");
